from ast import If
from flask import Flask, render_template, url_for, request

app = Flask(__name__)

@app.route("/")
def home():
    source = request.args.get('overlay')
    if source == '1':
                    # Left, Top, Right, Bottom
        pictureOne = [0, 0, 'null', 'null']
        pictureTwo = [300, 200, 'null', 'null']
        return render_template('index.html', value=source, pic1=pictureOne, pic2=pictureTwo)
    elif source == '2':
        return render_template('second.html', value=source)
    else:
        return render_template('index.html', value=source)

if __name__ == "__main__":
    app.run(debug=True)
